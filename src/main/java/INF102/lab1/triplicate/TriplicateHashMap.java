package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

// Time complexity: O(n)
public class TriplicateHashMap<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T element : list) {
            if (map.containsKey(element)) {
                map.put(element, map.get(element) + 1);
            } else {
                map.put(element, 1);
            }

            if (map.get(element) >= 3)
                return element;
        }
        return null;
    }

}
