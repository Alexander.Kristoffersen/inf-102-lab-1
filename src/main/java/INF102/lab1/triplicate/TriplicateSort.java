package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

// Time complexity: O(n*log(n))
public class TriplicateSort<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        // For each element check if it is followed by two equals
        Collections.sort(list);
        for (int i = 0; i < list.size() - 2; i++) {
            T a = list.get(i);
            T b = list.get(i + 1);
            T c = list.get(i + 2);
            if (a.equals(b) && b.equals(c)) {
                return a;
            }
        }
        return null;
    }

}
