package INF102.lab1.triplicate;

import java.util.HashSet;
import java.util.Set;
import java.util.List;

// Time complexity: O(n^2)
public class TriplicateFrequency<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Set<T> uniqueNumbers = new HashSet<>();
        for (int i = 0; i < list.size(); i++) {
            T a = list.get(i);
            //records frequency of all numbers in list
            if (uniqueNumbers.contains(a))
                continue;
            uniqueNumbers.add(a);
            int counter = 0;
            for (int j = i; j < list.size(); j++) {
                if (list.get(j).equals(a))
                    counter++;
                if (counter >= 3)
                    return a;
            }
        }
        return null;
    }

}
