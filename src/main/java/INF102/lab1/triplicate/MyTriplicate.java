package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {


    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> map = new HashMap<>();

        for (T element : list) {
            if (map.containsKey(element)) {
                map.put(element, map.get(element) + 1);
            } else {
                map.put(element, 1);
            }

            if (map.get(element) >= 3)
                return element;
        }
        return null;
    }

}
